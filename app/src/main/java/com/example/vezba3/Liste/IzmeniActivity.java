package com.example.vezba3.Liste;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.vezba3.Auto;
import com.example.vezba3.Baza.AutoDataBase;
import com.example.vezba3.R;
import com.squareup.picasso.Picasso;

public class IzmeniActivity extends AppCompatActivity {

    public static final int ZAHTEV_ZA_SLIKU = 1;

    private ImageButton slika;

    private EditText marka, model, cena, kubikaza;
    private Button izmeni;
    private AutoDataBase autoDataBase;
    private int id;
    private String url;

    private Auto auto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izmeni);

        id = getIntent().getIntExtra("id", -1);

        autoDataBase = new AutoDataBase(this);

        slika = findViewById(R.id.sliaImageButton1);
        marka = findViewById(R.id.markaEditTxt1);
        model = findViewById(R.id.modelEditTxt1);
        kubikaza = findViewById(R.id.kubikazaEditTxt1);
        cena = findViewById(R.id.cenaEditTxt1);
        izmeni = findViewById(R.id.izmeniBtn);



        auto = autoDataBase.nadjiAuto(id);

        if (auto.slika != null) {

            Picasso.get().load(Uri.parse(auto.slika)).fit().centerInside().into(slika);

        }else {

            Picasso.get().load(R.drawable.image).into(slika);

        }

        model.setText(auto.model);
        marka.setText(auto.marka);
        kubikaza.setText(String.valueOf(auto.kubikaza));
        cena.setText(String.valueOf(auto.cena));



        slika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                IzmeniActivity.this.startActivityForResult(intent, ZAHTEV_ZA_SLIKU);

            }
        });


        izmeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String modell = model.getText().toString();
                String markaa = marka.getText().toString();
                String kubikazaa = kubikaza.getText().toString();
                String cenaa = cena.getText().toString();

                try {

                    int k = Integer.parseInt(kubikazaa);
                    double c = Double.parseDouble(cenaa);
                    Auto a = null;

                    if (url != null && !url.equals("")) {

                        a = new Auto(id, url, markaa, modell, k, c);

                    } else {

                        a = new Auto(id, auto.slika, markaa, modell, k, c);

                    }



                    autoDataBase.izmeniAuto(a);
                    IzmeniActivity.this.finish();

                }
                catch (Exception e){

                    cena.setText("");
                    kubikaza.setText("");
                }
            }

        });


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ZAHTEV_ZA_SLIKU) {

            if (data != null) {

                Uri uri = data.getData();

                if (uri != null) {

                    url = uri.toString();
                    Picasso.get().load(uri).fit().centerInside().into(slika);

                }
                else {

                    Toast.makeText(this, "Nista nije izabrano", Toast.LENGTH_SHORT).show();

                }
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        slika = null;
        marka = null;
        model = null;
        cena = null;
        izmeni = null;
        auto = null;
        autoDataBase = null;
        url = null;

    }
}
