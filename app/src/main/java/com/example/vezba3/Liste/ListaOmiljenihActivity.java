package com.example.vezba3.Liste;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vezba3.Auto;
import com.example.vezba3.Baza.AutoDataBase;
import com.example.vezba3.MainActivity;
import com.example.vezba3.R;
import com.example.vezba3.Registracija_login.LoginActivity;
import com.example.vezba3.Registracija_login.Session;
import com.example.vezba3.User;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListaOmiljenihActivity extends AppCompatActivity {


    AdapterOmiljenih adapterOmiljenih;
    private RecyclerView recyclerView;
    ArrayList<Auto> auta;
    AutoDataBase autoDataBase;


    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView imeIPrezimeUser, emailUser;
    private ImageView slikaUser;

    private Session session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_omiljenih);
        setTitle("Lista omiljenih auta");

        recyclerView = findViewById(R.id.listaOmiljenih);

        session = new Session(this);



        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);


        drawerLayout = findViewById(R.id.drawer_layout2);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view2);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();

                if (id == R.id.listaId){

                    Intent intent = new Intent(ListaOmiljenihActivity.this, ListaActvity.class);
                    startActivity(intent);
                }
                else if (id == R.id.backListaFavAuta){


                    Intent intent = new Intent(ListaOmiljenihActivity.this, MainActivity.class);
                    startActivity(intent);

                }
                else if (id == R.id.logoutListaFavoritId){
                    session.setLogOut();
                    finish();
                    Intent intent = new Intent(ListaOmiljenihActivity.this, LoginActivity.class);
                    startActivity(intent);

                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }

        });


        View header = navigationView.getHeaderView(0);

        imeIPrezimeUser = header.findViewById(R.id.nav_ImeIPrezime);
        emailUser = header.findViewById(R.id.nav_email);
        slikaUser = header.findViewById(R.id.nav_Image);
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences sharedPreferences;
        Gson gson = new Gson();
        sharedPreferences = this.getSharedPreferences("myapp1", MODE_PRIVATE);
        String json = sharedPreferences.getString("userJson", "");
        User user = gson.fromJson(json, User.class);

        imeIPrezimeUser.setText(user.imeIPrezime);
        emailUser.setText(user.email);

        if (user.slika != null) {

            Uri uri = Uri.parse(user.slika);

            if (uri != null) {

                Picasso.get()
                        .load(uri)
                        .fit()
                        .centerInside()
                        .into(slikaUser);
            }

        } else{

            slikaUser.setImageResource(R.drawable.ic_osoba);

        }

        autoDataBase = new AutoDataBase(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        auta = (ArrayList<Auto>) autoDataBase.getFavAuta();
        adapterOmiljenih = new AdapterOmiljenih(auta, this);

        recyclerView.setAdapter(adapterOmiljenih);


    }
}
