package com.example.vezba3.Liste;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vezba3.Auto;
import com.example.vezba3.Baza.AutoDataBase;
import com.example.vezba3.R;
import com.example.vezba3.Slika.SlikaActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterOmiljenih extends RecyclerView.Adapter<AdapterOmiljenih.ViewHolder> {

    private ArrayList<Auto> auta;
    private Context context;
    private AutoDataBase autoDataBase;

    public AdapterOmiljenih(ArrayList<Auto> auta, Context context) {

        this.auta = auta;
        this.context = context;
        autoDataBase = new AutoDataBase(context);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_posebni_favorites, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Auto auto = auta.get(position);

        holder.model.setText(auto.model);
        holder.marka.setText(auto.marka);
        holder.cena.setText(String.valueOf(auto.cena));
        holder.kubikaza.setText(String.valueOf(auto.kubikaza));

        if (auto.slika != null) {

            Uri uri = Uri.parse(auto.slika);
            Picasso.get().load(uri).fit().centerInside().into(holder.imageView);

        } else {

            Picasso.get().load(R.drawable.image).into(holder.imageView);

        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SlikaActivity.class);
                intent.putExtra("slika", auto.slika);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return auta.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView marka, model, cena, kubikaza;
        ImageView imageView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.slikaOmi);

            marka = itemView.findViewById(R.id.markaOmi);
            model = itemView.findViewById(R.id.modelOmi);
            cena = itemView.findViewById(R.id.cenaOmi);
            kubikaza = itemView.findViewById(R.id.kubikazaOmi);
        }
    }
}
