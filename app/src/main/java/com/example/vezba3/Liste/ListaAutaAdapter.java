package com.example.vezba3.Liste;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.vezba3.Auto;
import com.example.vezba3.Baza.AutoDataBase;
import com.example.vezba3.R;
import com.example.vezba3.Slika.SlikaActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListaAutaAdapter extends RecyclerView.Adapter<ListaAutaAdapter.ViewHolder> {

    private ArrayList<Auto> auta;
    private Context context;
    private AutoDataBase autoDataBase;

    public ListaAutaAdapter(ArrayList<Auto> auta, Context context) {

        this.auta = auta;
        this.context = context;
        autoDataBase = new AutoDataBase(context);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_one_list_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }



    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        viewHolder.linearLayout.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_away));

        final Auto auto = auta.get(i);
        if (auto.slika != null) {

            Picasso.get().load(Uri.parse(auto.slika)).fit().centerInside().into(viewHolder.imageView);

        } else {

            Picasso.get().load(R.drawable.image).into(viewHolder.imageView);

        }
        viewHolder.model.setText(auto.model);
        viewHolder.marka.setText(auto.marka);
        viewHolder.kubikaza.setText(String.valueOf(auto.kubikaza));
        viewHolder.cena.setText(String.valueOf(auto.cena));

        if (auto.isFavorite == 1){

            viewHolder.toggleButton.setChecked(true);

        }
        else{

            viewHolder.toggleButton.setChecked(false);

        }



        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SlikaActivity.class);
                intent.putExtra("slika", auto.slika);
                context.startActivity(intent);

            }
        });


        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Dialog")
                        .setIcon(R.drawable.ic_upozorenje)
                        .setMessage("Izaberite opciju")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        })
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                autoDataBase.deleteAuto(auto.id);
                                auta.remove(auto);
                                ListaAutaAdapter.this.notifyDataSetChanged();

                            }
                        })
                        .setNeutralButton("Izmeni", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent i = new Intent(context, IzmeniActivity.class);
                                i.putExtra("id", auto.id);
                                context.startActivity(i);

                            }
                        })
                        .create()
                        .show();

                return false;

            }

        });



        viewHolder.toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    autoDataBase.UpdateFavorite(auto.id,1);

                } else {

                    autoDataBase.UpdateFavorite(auto.id,0);

                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return auta.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView model, marka, kubikaza, cena;
        private ToggleButton toggleButton;
        private LinearLayout linearLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            model = itemView.findViewById(R.id.modelTextView);
            marka = itemView.findViewById(R.id.markaTextView);
            kubikaza = itemView.findViewById(R.id.kubikazaTextView);
            cena = itemView.findViewById(R.id.cenaTextView);

            toggleButton = itemView.findViewById(R.id.toggleBtn);
            linearLayout = itemView.findViewById(R.id.lineaLayout);


        }
    }



}
