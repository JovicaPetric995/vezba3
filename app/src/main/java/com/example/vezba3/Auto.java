package com.example.vezba3;

import java.io.Serializable;

public class Auto implements Serializable {

    public String slika;
    public String marka;
    public String model;
    public Integer kubikaza;
    public double cena;
    public Integer id;
    public Integer isFavorite;

    public Auto(String slika, String marka, String model, Integer kubikaza, double cena) {

        this.slika = slika;
        this.marka = marka;
        this.model = model;
        this.kubikaza = kubikaza;
        this.cena = cena;

    }

    public Auto(Integer id, String slika, String marka, String model, Integer kubikaza, double cena) {

        this.id = id;
        this.slika = slika;
        this.marka = marka;
        this.model = model;
        this.kubikaza = kubikaza;
        this.cena = cena;

    }


    public Auto(Integer id, String slika, String marka, String model, Integer kubikaza, double cena, Integer isFavorite) {

        this.id = id;
        this.slika = slika;
        this.marka = marka;
        this.model = model;
        this.kubikaza = kubikaza;
        this.cena = cena;
        this.isFavorite = isFavorite;

    }

    @Override
    public String toString() {

        return "Auto{" +
                ", marka='" + marka + '\'' +
                ", model='" + model + '\'' +
                ", kubikaza=" + kubikaza +
                ", cena=" + cena +
                ", id=" + id +
                '}';
    }
}
