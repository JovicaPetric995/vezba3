package com.example.vezba3;

import java.io.Serializable;

public class User implements Serializable {


    public int id;
    public String imeIPrezime;
    public String email;
    public String password;
    public String slika;

    public User(String imeIPrezime, String email, String password, String slika) {

        this.imeIPrezime = imeIPrezime;
        this.email = email;
        this.password = password;
        this.slika = slika;

    }

    public User(int id, String imeIPrezime, String email, String password, String slika) {

        this.id = id;
        this.imeIPrezime = imeIPrezime;
        this.email = email;
        this.password = password;
        this.slika = slika;

    }
}
