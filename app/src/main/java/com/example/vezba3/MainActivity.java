package com.example.vezba3;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vezba3.Baza.AutoDataBase;
import com.example.vezba3.Fragment.MinMaxActivity;
import com.example.vezba3.Liste.ListaActvity;
import com.example.vezba3.Liste.ListaOmiljenihActivity;

import com.example.vezba3.Registracija_login.LoginActivity;
import com.example.vezba3.Registracija_login.Session;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final int ZAHTEV_ZA_SLIKU = 1;
    public static final int ZAHTEV_ZA_PERMISSION = 2;

    private EditText model, marka, kubikaza, cena;
    private ImageButton izborSlike;
    private Button unos, prikaz, izdvoj, logout;
    private TextView imeIPrezime, email;
    private ImageView slika;

    private String url;
    private AutoDataBase autoDataBase;
    private boolean isPermissionGranted;

    private ArrayList<Auto> auta;

    private Session session;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Pocetna strana");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        izborSlike = findViewById(R.id.sliaImageButton);

        model = findViewById(R.id.modelEditTxt);
        marka = findViewById(R.id.markaEditTxt);
        kubikaza = findViewById(R.id.kubikazaEditTxt);
        cena = findViewById(R.id.cenaEditTxt);

        unos = findViewById(R.id.unosBtn);
        prikaz = findViewById(R.id.prukazBtn);
        izdvoj = findViewById(R.id.izdvojBtn);
        logout = findViewById(R.id.logoutBtn);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);


        imeIPrezime = header.findViewById(R.id.nav_ImeIPrezime);
        email = header.findViewById(R.id.nav_email);
        slika = header.findViewById(R.id.nav_Image);

        autoDataBase = new AutoDataBase(this);
        auta = autoDataBase.getAuta();


        session = new Session(this);
        if (!session.loggedin()){

            logout();

        }

        //Dugme za unos podataka u listu

        unos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mod = model.getText().toString().trim();
                String mar = marka.getText().toString().trim();
                String kub = kubikaza.getText().toString().trim();
                String cen = cena.getText().toString().trim();

                //validacije za svaki podatak da se unese!!!

                if (model == null || model.equals("")){
                    Toast.makeText(MainActivity.this, "Unesite model auta!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (marka == null || marka.equals("")){
                    Toast.makeText(MainActivity.this, "Unesite marku auta!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (kubikaza == null || kubikaza.equals("")){
                    Toast.makeText(MainActivity.this, "Unesite kubikazu auta!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (cena == null || cena.equals("")){
                    Toast.makeText(MainActivity.this, "Unesite cenu auta!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //try i catch za unet string podatak u kubikazu i cenu!!

                try {

                    Integer k = Integer.parseInt(kub);
                    Double c = Double.parseDouble(cen);
                    Auto auto = new Auto(url, mod, mar, k, c);

                    autoDataBase.unosAuta(auto);
                    auta.add(auto);

                    url = "" ;
                    marka.setText("");
                    model.setText("");
                    kubikaza.setText("");
                    cena.setText("");
                    izborSlike.setImageResource(R.drawable.image);

                } catch (Exception e) {

                    kubikaza.setText("");
                    cena.setText("");
                }

            }
        });

        // Dugme za izbor slike!

        izborSlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isPermissionGranted) {
                    requestAndCheckPermission();
                    return;
                }

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                MainActivity.this.startActivityForResult(intent, ZAHTEV_ZA_SLIKU);

            }
        });

        //Dugme za prikaz Liste u recyce view

        prikaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPermissionGranted) {

                    requestAndCheckPermission();
                    return;

                }

                final User user = (User) getIntent().getSerializableExtra("user");

                Intent intent = new Intent(MainActivity.this, ListaActvity.class);
                intent.putExtra("lista", auta);
                intent.putExtra("user", user);
                MainActivity.this.startActivity(intent);

            }
        });

        //Dugme za prikaz podataka najskupljeg i najjeftinijeg auta!

        izdvoj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                auta = autoDataBase.getAuta();

                if (auta != null && auta.size() >0){

                    Auto min = auta.get(0);
                    Auto max = auta.get(0);
                    for (int i = 1; i<auta.size(); i++){

                        Auto auto = auta.get(i);

                        if (min.cena > auto.cena) min = auto;
                        if(max.cena < auto.cena) max = auto;

                    }

                    Intent intent = new Intent(MainActivity.this, MinMaxActivity.class);
                    intent.putExtra("min", min);
                    intent.putExtra("max", max);
                    startActivity(intent);

                } else {

                    Toast.makeText(MainActivity.this, "Prvo unesite auta!", Toast.LENGTH_SHORT).show();

                }

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();

            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();
        requestAndCheckPermission();

        //getUser
        SharedPreferences sharedPreferences;
        Gson gson = new Gson();
        sharedPreferences = this.getSharedPreferences("myapp1", MODE_PRIVATE);
        String json = sharedPreferences.getString("userJson", "");
        User user = gson.fromJson(json, User.class);


        imeIPrezime.setText(user.imeIPrezime);
        email.setText(user.email);

        if (user.slika != null) {

            Uri uri = Uri.parse(user.slika);
            if (uri != null) {
                Picasso.get()
                        .load(uri)
                        .fit()
                        .centerInside()
                        .into(slika);
            }

        } else{

            slika.setImageResource(R.drawable.ic_osoba);
        }


    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){

            drawerLayout.closeDrawer(GravityCompat.START);

        }
        else {

            super.onBackPressed();
        }
    }






    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == ZAHTEV_ZA_SLIKU && resultCode == RESULT_OK) {

            if (data != null) {

                Uri uri = data.getData();

                if (uri != null) {

                    url = uri.toString();
                    Picasso.get().load(uri).fit().centerInside().into(izborSlike);

                } else {

                    Toast.makeText(this, "Nista nije selektovano!", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == ZAHTEV_ZA_PERMISSION){

            if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                isPermissionGranted = true;
            }
            else {

                isPermissionGranted = false;
            }
        }
    }

    private void requestAndCheckPermission() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            isPermissionGranted = false;
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Permisson")
                        .setIcon(R.drawable.ic_upozorenje)
                        .setMessage("Da bi aplikacija mogla da pristupi slikama sa uredjaja, moramo dozvoliti permisson.")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Dozvoli permisson", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, ZAHTEV_ZA_PERMISSION);
                                dialog.dismiss();

                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, ZAHTEV_ZA_PERMISSION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            isPermissionGranted = true;
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();

        if (id == R.id.autoId){

            final User user = (User) getIntent().getSerializableExtra("user");
            Intent intent = new Intent(MainActivity.this, ListaActvity.class);
            intent.putExtra("lista", auta);
            intent.putExtra("user", user);
            MainActivity.this.startActivity(intent);

        }
        else if (id == R.id.favAutoId){

            final User user = (User) getIntent().getSerializableExtra("user");
            Intent intent = new Intent(MainActivity.this, ListaOmiljenihActivity.class);
            intent.putExtra("user", user);
            MainActivity.this.startActivity(intent);

        }
        else if (id == R.id.logOutId){

            logout();

        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout(){

        session.setLogOut();
        finish();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        model = null;
        marka = null;
        kubikaza = null;
        cena = null;
        izborSlike = null;
        unos = null;
        prikaz = null;
        izdvoj = null;
        logout = null;
        imeIPrezime = null;
        slika = null;
        email = null;
        url = null;
        autoDataBase = null;
        session = null;
        drawerLayout = null;
        navigationView = null;
        auta = null;

    }
}
