package com.example.vezba3.Slika;

import android.net.Uri;
import android.os.Bundle;
import com.example.vezba3.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import androidx.appcompat.app.AppCompatActivity;


public class SlikaActivity extends AppCompatActivity {

    private PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slika);

        setTitle("Slika auta");
        photoView = findViewById(R.id.viewPhoto);

        final String slika = getIntent().getStringExtra("slika");

        if (slika != null) {

            Uri uri = Uri.parse(slika);

            if (uri != null) {

                Picasso.get()
                        .load(uri)
                        .fit()
                        .centerInside()
                        .into(photoView);

            }

        }else {

            Picasso.get()
                    .load(R.drawable.image)
                    .fit()
                    .centerInside()
                    .into(photoView);

        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        photoView = null;

    }
}
