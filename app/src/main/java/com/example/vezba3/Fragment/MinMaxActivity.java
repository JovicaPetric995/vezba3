package com.example.vezba3.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vezba3.Auto;
import com.example.vezba3.Liste.ListaActvity;
import com.example.vezba3.Liste.ListaOmiljenihActivity;
import com.example.vezba3.MainActivity;
import com.example.vezba3.R;
import com.example.vezba3.Registracija_login.LoginActivity;
import com.example.vezba3.Registracija_login.Session;
import com.example.vezba3.User;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MinMaxActivity extends AppCompatActivity {

    private Auto autoMin, autoMax;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView imeIPrezimeUser, emailUser;
    private ImageView slikaUser;

    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_min_max);

        Toolbar toolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);

        session = new Session(this);

        drawerLayout = findViewById(R.id.drawer_layout3);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view3);

        View header = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if (id == R.id.autoId) {

                    Intent intent = new Intent(MinMaxActivity.this, ListaActvity.class);
                    MinMaxActivity.this.startActivity(intent);
                } else if (id == R.id.favAutoId) {

                    Intent intent = new Intent(MinMaxActivity.this, ListaOmiljenihActivity.class);
                    MinMaxActivity.this.startActivity(intent);

                } else if (id == R.id.logOutId) {

                    session.setLogOut();
                    finish();
                    Intent intent = new Intent(MinMaxActivity.this, LoginActivity.class);
                    startActivity(intent);

                }

                drawerLayout.closeDrawer(GravityCompat.START);

                return true;
            }
        });

        imeIPrezimeUser = header.findViewById(R.id.nav_ImeIPrezime);
        emailUser = header.findViewById(R.id.nav_email);
        slikaUser = header.findViewById(R.id.nav_Image);

        autoMin = (Auto) getIntent().getSerializableExtra("min");
        autoMax = (Auto) getIntent().getSerializableExtra("max");


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.minContainer, AutoFragment.newInstance("Najjeftiniji auto", autoMin));
        fragmentTransaction.add(R.id.maxContainer, AutoFragment.newInstance("Najskuplji auto", autoMax));
        fragmentTransaction.commit();

    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences sharedPreferences;
        Gson gson = new Gson();
        sharedPreferences = this.getSharedPreferences("myapp1", MODE_PRIVATE);
        String json = sharedPreferences.getString("userJson", "");
        User user = gson.fromJson(json, User.class);

        imeIPrezimeUser.setText(user.imeIPrezime);
        emailUser.setText(user.email);

        if (user.slika != null) {

            Uri uri = Uri.parse(user.slika);

            if (uri != null) {

                Picasso.get()
                        .load(uri)
                        .fit()
                        .centerInside()
                        .into(slikaUser);
            }

        } else{

            slikaUser.setImageResource(R.drawable.ic_osoba);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        autoMin = null;
        autoMax = null;
        drawerLayout = null;
        navigationView = null;
        slikaUser = null;
        imeIPrezimeUser = null;
        emailUser = null;
        session = null;

    }
}
