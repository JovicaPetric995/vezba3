package com.example.vezba3.Fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vezba3.Auto;
import com.example.vezba3.R;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AutoFragment extends Fragment {

    private TextView title, marka, model, kubikaza, cena;
    private ImageView slikaAuta;
    private String titleText;
    private Auto auto;




    public static AutoFragment newInstance(String title, Auto auto) {

        AutoFragment fragment = new AutoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putSerializable("auto", auto);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        titleText = arguments.getString("title");
        auto = (Auto) arguments.getSerializable("auto");
}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_auto, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title = view.findViewById(R.id.titleTextView);
        marka = view.findViewById(R.id.markaFragmentAuto);
        model = view.findViewById(R.id.modelFragmentAuto);
        kubikaza = view.findViewById(R.id.kubikazaFragmentAuto);
        cena = view.findViewById(R.id.cenaFragmentAuto);
        slikaAuta = view.findViewById(R.id.slikaFragmentAuto);


        if (auto.slika != null) {

            Picasso.get().load(Uri.parse(auto.slika)).fit().centerInside().into(slikaAuta);

        }else {

            Picasso.get().load(R.drawable.image).into(slikaAuta);

        }

        title.setText(titleText);
        String textMarka = "Marka auta: " +auto.marka;
        String textModel = "Marka auta: " +auto.model;
        String textKubikaza = "Kubikaza auta: " +auto.kubikaza;
        String textCena =  "Cena auta: " +auto.cena;

        marka.setText(textMarka);
        model.setText(textModel);
        kubikaza.setText(textKubikaza);
        cena.setText(textCena);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        title = null;
        marka = null;
        model = null;
        cena = null;
        kubikaza = null;
        slikaAuta = null;
        auto = null;
        titleText = null;
    }
}
