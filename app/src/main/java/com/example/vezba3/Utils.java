package com.example.vezba3;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static ArrayList<Auto> listaAuta= null;

    public static ArrayList<Auto> getListaAuta(){

        if(listaAuta == null){

            listaAuta = new ArrayList<>();

        }

        return listaAuta;
    }


    public static void removeAuto(Auto auto){

        if(listaAuta != null && listaAuta.contains(auto)){

            listaAuta.remove(auto);
        }
    }

    public static void setListaAuta(ArrayList<Auto> listaAuta) {

        ArrayList<Auto> lista = getListaAuta();
        lista.clear();
        lista.addAll(listaAuta);

    }

    public static List<Auto> getFavoritesCars() {

        ArrayList<Auto> favoriteCars = new ArrayList<>();

        for (Auto auto : listaAuta) {

            if (auto.isFavorite == 1 ) {

                favoriteCars.add(auto);

            }

        }

        return favoriteCars;

    }

}
