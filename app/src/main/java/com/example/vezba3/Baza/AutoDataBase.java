package com.example.vezba3.Baza;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vezba3.Auto;

import java.util.ArrayList;
import java.util.List;

public class AutoDataBase extends SQLiteOpenHelper {


    public static final String IME_BAZE = "auto_baza.db";

    public static final String TABELA = "auto";
    public static final String KOLONA_ID = "id";
    public static final String KOLONA_SLIKA = "slika";
    public static final String KOLONA_MARKA = "marka";
    public static final String KOLONA_MODEL = "model";
    public static final String KOLONA_KUBIKAZA = "kubikaza";
    public static final String KOLONA_CENA = "cena";
    public static final String KOLONA_ISFAVORITE = "isFavorite";




    public AutoDataBase( Context context) {
        super(context, IME_BAZE, null, 2);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " +TABELA + "(" +"id integer primary key autoincrement, "
                + ""+KOLONA_SLIKA+ " text,"
                + KOLONA_MARKA + " text,"
                + KOLONA_MODEL +" text,"
                + KOLONA_KUBIKAZA + " integer,"
                + KOLONA_CENA + " real, "
                + KOLONA_ISFAVORITE + " integer );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABELA);
        onCreate(db);

    }

    public boolean unosAuta(Auto auto){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KOLONA_ID, auto.id);
        contentValues.put(KOLONA_SLIKA, auto.slika);
        contentValues.put(KOLONA_MODEL, auto.model);
        contentValues.put(KOLONA_MARKA, auto.marka);
        contentValues.put(KOLONA_KUBIKAZA, auto.kubikaza);
        contentValues.put(KOLONA_CENA, auto.cena);
        contentValues.put(KOLONA_ISFAVORITE, auto.isFavorite);


        long result = db.insert(TABELA, null, contentValues);
        if (result == -1){

            return false;
        }

        return true;

    }

    public ArrayList<Auto> getAuta(){

        String query = "select * from " +TABELA;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<Auto> auta = new ArrayList<>();

        if(cursor.getCount() > 0){

            while(cursor.moveToNext()){

                Auto a = new Auto(cursor.getInt(0),
                                cursor.getString(1),
                                cursor.getString(2),
                                cursor.getString(3),
                                cursor.getInt(4),
                                cursor.getDouble(5),
                                cursor.getInt(6)
                );

                auta.add(a);

            }
        cursor.close();


        }
        return auta;
    }

    public Integer deleteAuto(Integer id){

        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete(TABELA, "ID= ?", new String[]{String.valueOf(id)});
        return i;

    }

    public boolean izmeniAuto(Auto auto){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KOLONA_ID, auto.id);
        contentValues.put(KOLONA_SLIKA, auto.slika);
        contentValues.put(KOLONA_MODEL, auto.model);
        contentValues.put(KOLONA_MARKA, auto.marka);
        contentValues.put(KOLONA_KUBIKAZA, auto.kubikaza);
        contentValues.put(KOLONA_CENA, auto.cena);
        contentValues.put(KOLONA_ISFAVORITE, auto.isFavorite);

        db.update(TABELA, contentValues, "id = ?", new String[]{String.valueOf(auto.id)});

        return true;
    }

    public Auto nadjiAuto(int id){

        ArrayList<Auto> auta = getAuta();

        for (Auto a : auta) {

            if (a.id == id) {

                return a;
            }
        }

        return null;

    }



    public  List<Auto> getFavAuta(){

        ArrayList<Auto> auta = new ArrayList<>();

        for (Auto auto : getAuta() ){

            if (auto.isFavorite == 1){

                auta.add(auto);
            }
        }

        return auta;

    }

    public boolean UpdateFavorite(int id, int isFavorite){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KOLONA_ISFAVORITE, isFavorite);

        db.update(TABELA, contentValues, "id =?", new String[]{String.valueOf(id)});
        return true;

    }
}

