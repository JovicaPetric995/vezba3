package com.example.vezba3.Registracija_login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vezba3.MainActivity;
import com.example.vezba3.R;
import com.example.vezba3.User;
import com.google.gson.Gson;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button login, registracija;
    private EditText etEmail, etPassword;

    private Session session;
    private DbUser1 dbUser1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle("Login/registracija");

        login = findViewById(R.id.btnLogin);
        registracija = findViewById(R.id.btnRegistracija);
        login.setOnClickListener(this);
        registracija.setOnClickListener(this);

        etEmail = findViewById(R.id.editTextEmail);
        etPassword = findViewById(R.id.editTextPass);

        session = new Session(this);
        dbUser1 = new DbUser1(this);


        if (session.loggedin()){

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btnLogin

                    : login();

            break;

            case R.id.btnRegistracija
                    :
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);

            break;

        }

    }

    private void login() {

        String email = etEmail.getText().toString().trim();
        String pass = etPassword.getText().toString().trim();
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;

        User user = dbUser1.getUser(email);

        if (user != null) {

            if (user.password.equals(pass)) {

                sharedPreferences = this.getSharedPreferences("myapp1", MODE_PRIVATE);
                editor = sharedPreferences.edit();
                Gson gson = new Gson();
                String json = gson.toJson(user);
                editor.putString("userJson", json);
                editor.commit();

                session.setLoggedIn(json);

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                finish();
                startActivity(intent);

            } else {

                Toast.makeText(this, "Wrong password", Toast.LENGTH_SHORT).show();

            }
        } else {

            Toast.makeText(this, "Wrong email", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        login = null;
        registracija = null;
        etEmail = null;
        etPassword = null;
        session = null;
        dbUser1 = null;

    }
}
