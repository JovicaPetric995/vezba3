package com.example.vezba3.Registracija_login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vezba3.R;
import com.example.vezba3.User;
import com.squareup.picasso.Picasso;

import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    public static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least one number
                    "(?=.*[a-z])" +         //at least one lower case caracter
                    "(?=.*[A-Z])" +         //at least one upper case caracter
//                    "(?=.*[@#$%^&+=])" +    //at least 1 special caracter
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 6 caracters
                    "$");

    private Button reg;
    private EditText etEmail, etPass, etConfirmPass, imeIPrezime;
    private TextView tvLogin;
    private ImageButton slika;
    private String url;
    public static final int ZAHTEV_ZA_SLIKU_1 = 1;

    private DbUser1 dbUser1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("Registracija");

        reg = findViewById(R.id.btnRegistracijaReg);
        etEmail = findViewById(R.id.editTextEmailReg);
        etPass = findViewById(R.id.editTextPassReg);
        tvLogin = findViewById(R.id.tvLogin);
        etConfirmPass = findViewById(R.id.editTextConfirmPass);
        imeIPrezime = findViewById(R.id.imeIPrezime);
        slika = findViewById(R.id.osobaImageButton);

        dbUser1 = new DbUser1(this);

        reg.setOnClickListener(this);
        tvLogin.setOnClickListener(this);

        slika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                RegisterActivity.this.startActivityForResult(intent, ZAHTEV_ZA_SLIKU_1);
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnRegistracijaReg:
                register();
                break;

            case R.id.tvLogin:
                finish();
                break;

             default:
        }

    }

    private void register(){

        String email = etEmail.getText().toString().trim();
        String pass = etPass.getText().toString().trim();
        String confirmPass = etConfirmPass.getText().toString().trim();
        String imeIPre = imeIPrezime.getText().toString();

        User user = new User(imeIPre, email, pass, url);

        if (email.isEmpty() && pass.isEmpty() && confirmPass.isEmpty() && imeIPre.isEmpty()) {

            Toast.makeText(this, "Username/password/confirm password polje je prazno", Toast.LENGTH_SHORT).show();

        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {

            Toast.makeText(this, "Enter valid email adress", Toast.LENGTH_SHORT).show();

        } else if (!PASSWORD_PATTERN.matcher(pass).matches()) {

            Toast.makeText(this, "Password too weak(at least 6 caracters) /one digit/ one uppercase ", Toast.LENGTH_SHORT).show();

        } else if (email.equals(dbUser1.getUserEmail())) {

            Toast.makeText(this, "Email alredy exists", Toast.LENGTH_SHORT).show();

        } else if (!pass.equals(confirmPass)) {

            Toast.makeText(this, "Please reenter password correctly", Toast.LENGTH_SHORT).show();

        } else {

            dbUser1.addUser(user);
            Toast.makeText(this, "User registrovan", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ZAHTEV_ZA_SLIKU_1){

            if (resultCode == RESULT_OK){

                Uri uri = data.getData();

                if (data != null){

                    url = uri.toString();
                    Picasso.get().load(uri).fit().centerInside().into(slika);

                }
            }
        }

    }
}
