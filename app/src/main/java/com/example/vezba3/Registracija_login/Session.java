package com.example.vezba3.Registracija_login;


import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.vezba3.User;

import java.util.ArrayList;

public class Session {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;


    public Session (Context context){

        this.context = context;
        sharedPreferences = context.getSharedPreferences("myapp1", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    public void setLoggedIn(String userJson){

        editor.putString("userJson", userJson);
        editor.commit();

    }

    public void setLogOut() {

        editor.putString("userJson", null);
        editor.commit();

    }

    public boolean loggedin() {

        String userJson = sharedPreferences.getString("userJson", null);
        if (userJson != null && !userJson.equals("")) {

            return true;

        }
        return false;
    }


}
