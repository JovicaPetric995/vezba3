package com.example.vezba3.Registracija_login;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vezba3.User;



public class DbUser1 extends SQLiteOpenHelper {

    public static final String DB_NAME = "myapp1";
    public static final int DB_VERSION = 2;

    public static final String TABELA = "users";
    public static final String KOLONA_ID = "id";
    public static final String KOLONA_IME_I_PREZIME = "imeIPrezime";
    public static final String KOLONA_EMAIl = "email";
    public static final String KOLONA_PASSWORD = "password";
    public static final String KOLONA_SLIKA = "slika";

    public static final String CREATE_TABLE_USERS = "CREATE TABLE "+ TABELA + "("
            +KOLONA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            +KOLONA_EMAIl + " TEXT,"
            +KOLONA_PASSWORD + " TEXT, "
            +KOLONA_SLIKA + " TEXT,"
            +KOLONA_IME_I_PREZIME + " TEXT);";




    public DbUser1(Context context) {

        super(context, DB_NAME, null, DB_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_USERS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(" drop table if exists " +TABELA);
        onCreate(db);

    }

    public void addUser(User user){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KOLONA_EMAIl, user.email);
        contentValues.put(KOLONA_PASSWORD, user.password);
        contentValues.put(KOLONA_IME_I_PREZIME, user.imeIPrezime);
        contentValues.put(KOLONA_SLIKA, user.slika);

        db.insert(TABELA, null, contentValues);
        db.close();

    }

    public User getUser(String email){


        String query = "select * from " + TABELA + " where " + KOLONA_EMAIl + " = " + "'" +email+ "'";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        User user = null;

        if (cursor.getCount() > 0){

            cursor.moveToFirst();
            user = new User(cursor.getString(4),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3));

        }

        cursor.close();

        return user;

    }



    public String getUserEmail(){

        String email = null;
        String query = " select email from " + TABELA + " where " + KOLONA_EMAIl + " = " + "'" +email+ "'";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {

            cursor.moveToFirst();
            while(cursor.isAfterLast()){

                return email;

            }

            cursor.moveToNext();

        }

        return query;
    }



}
