package com.example.vezba3.SortiranjeListe;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vezba3.Auto;
import com.example.vezba3.Baza.AutoDataBase;
import com.example.vezba3.Liste.AdapterOmiljenih;
import com.example.vezba3.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragmentOpadajuci extends Fragment {

    private RecyclerView recyclerView;
    private AutoDataBase autoDatabase;
    private AdapterOmiljenih adapter;
    private ArrayList<Auto> auta = new ArrayList<>();
    private View view;

    public  FragmentOpadajuci(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_opadajuci, container, false);
        autoDatabase = new AutoDataBase(getContext());


        recyclerView = view.findViewById(R.id.listaOpadajucih);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        auta = autoDatabase.getAuta();

        Collections.sort(auta, new Comparator<Auto>() {
            @Override
            public int compare(Auto o1, Auto o2) {

                return Double.compare(o2.cena, o1.cena);

            }
        });

        adapter = new AdapterOmiljenih(auta, getContext());
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        recyclerView = null;
        autoDatabase = null;
        adapter = null;
        auta = null;
        view = null;

    }
}
