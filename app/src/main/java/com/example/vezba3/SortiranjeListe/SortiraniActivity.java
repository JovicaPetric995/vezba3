package com.example.vezba3.SortiranjeListe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vezba3.Liste.ListaActvity;
import com.example.vezba3.Liste.ListaOmiljenihActivity;
import com.example.vezba3.MainActivity;
import com.example.vezba3.R;
import com.example.vezba3.Registracija_login.LoginActivity;
import com.example.vezba3.Registracija_login.Session;
import com.example.vezba3.User;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class SortiraniActivity extends AppCompatActivity {


    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;


    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private TextView imeIPrezimeUser, emailUser;
    private ImageView slikaUser;

    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sortirani);

        setTitle("Sortirana auta");
        session = new Session(this);

        //navigacioni meni

        Toolbar toolbar = findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout4);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view4);
        View header = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();

                if (id == R.id.autoId) {

                    Intent intent = new Intent(SortiraniActivity.this, ListaActvity.class);
                    startActivity(intent);

                } else if (id == R.id.favAutoId) {

                    Intent intent = new Intent(SortiraniActivity.this, ListaOmiljenihActivity.class);
                    SortiraniActivity.this.startActivity(intent);

                } else if (id == R.id.logOutId) {

                    session.setLogOut();
                    finish();
                    Intent intent = new Intent(SortiraniActivity.this, LoginActivity.class);
                    startActivity(intent);

                }

                drawerLayout.closeDrawer(GravityCompat.START);

                return true;

            }

        });

        imeIPrezimeUser = header.findViewById(R.id.nav_ImeIPrezime);
        emailUser = header.findViewById(R.id.nav_email);
        slikaUser = header.findViewById(R.id.nav_Image);

        //tab layout i viewpager

        tabLayout = findViewById(R.id.tabid);
        appBarLayout = findViewById(R.id.appBarId);
        viewPager = findViewById(R.id.viewPagerId);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new FragmentOpadajuci(), "Lista po opadajucem");
        adapter.AddFragment(new FragmentRastuci(), "Lista po rastucem");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences sharedPreferences;
        Gson gson = new Gson();
        sharedPreferences = this.getSharedPreferences("myapp1", MODE_PRIVATE);
        String json = sharedPreferences.getString("userJson", "");
        User user = gson.fromJson(json, User.class);

        imeIPrezimeUser.setText(user.imeIPrezime);
        emailUser.setText(user.email);

        if (user.slika != null) {

            Uri uri = Uri.parse(user.slika);

            if (uri != null) {

                Picasso.get()
                        .load(uri)
                        .fit()
                        .centerInside()
                        .into(slikaUser);
            }

        } else {

            slikaUser.setImageResource(R.drawable.ic_osoba);

        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        tabLayout = null;
        appBarLayout = null;
        viewPager = null;
        drawerLayout = null;
        navigationView = null;
        imeIPrezimeUser = null;
        slikaUser = null;
        emailUser = null;
        session = null;

    }
}
